/* eslint-disable @typescript-eslint/no-unused-vars */

import { useEffect, useState, useReducer } from 'preact/hooks'
import { Subject } from 'rxjs'
import * as ops from 'rxjs/operators'

type EventState = Readonly<{
  inbox: Readonly<{ x: number; y: number }> | null
  primary: boolean
  secondary: boolean
  shiftKey: boolean
  log: boolean
}>

type EventInput =
  | readonly ['down', PointerEvent]
  | readonly ['move', PointerEvent]
  | readonly ['up', PointerEvent]
  | readonly ['cancel', PointerEvent]
  | readonly ['out', PointerEvent]
  | readonly ['leave', PointerEvent]
  | readonly ['enter', PointerEvent]

/** @internal */
const unreachable = (_v: never) => {
  throw new Error('unreachable.')
}

const px2num = (pxSize: string) => {
  const m = pxSize.match(/^(\d+)px$/)
  if (m) return Number(m[1] || 0)
  throw new Error('error: unknown size.')
  // return 0
}

const getPosition = (e: PointerEvent) => {
  if (e.currentTarget instanceof Element) {
    const rect = e.currentTarget.getBoundingClientRect()
    const size = px2num(getComputedStyle(e.currentTarget).fontSize) / 2
    const x = Math.floor((e.clientX - rect.left) / size)
    const y = Math.floor((e.clientY - rect.top) / size)
    return { x, y } as const
  }
  throw new Error("error: can't get position.")
}

const init: EventState = {
  inbox: null,
  primary: false,
  secondary: false,
  shiftKey: false,
  log: false,
}
const reducer = (state: EventState, e: EventInput): EventState => {
  const { shiftKey } = e[1]
  switch (e[0]) {
    case 'cancel':
      return { ...init, shiftKey, log: false }
    case 'down':
    case 'up': {
      const button =
        'mouse' !== e[1].pointerType || 0 === e[1].button
          ? 'primary'
          : 2 === e[1].button
            ? 'secondary'
            : null
      const push = 'up' !== e[0]
      if (button) return { ...state, [button]: push, shiftKey, log: true }
      return { ...state, shiftKey, log: true }
    }
    case 'enter':
    case 'move':
      return { ...state, inbox: getPosition(e[1]), shiftKey, log: false }
    case 'out':
    case 'leave':
      return { ...state, inbox: null, shiftKey, log: false }
    default:
      return unreachable(e)
  }
}

const event$ = new Subject<EventInput>()
const pointer$ = event$.pipe(ops.scan(reducer, init))
const click$ = new Subject<'click' | 'dblclick'>()

const push = (e: EventInput) => {
  const log = 'up' === e[0] || 'down' === e[0]
  if (log) console.log('start')
  event$.next(e)
  if (log) console.log('end')
}

const handlers = {
  onClick: (e: MouseEvent) => {
    e.preventDefault()
    click$.next('click')
  },
  onDblClick: (e: MouseEvent) => {
    e.preventDefault()
    click$.next('dblclick')
  },
  onPointerDown: (e: PointerEvent) => push(['down', e]),
  onPointerMove: (e: PointerEvent) => push(['move', e]),
  onPointerUp: (e: PointerEvent) => push(['up', e]),
  onPointerCancel: (e: PointerEvent) => push(['cancel', e]),
  onPointerOut: (e: PointerEvent) => push(['out', e]),
  onPointerLeave: (e: PointerEvent) => push(['leave', e]),
  onPointerEnter: (e: PointerEvent) => push(['enter', e]),
}

const styles = {
  wrap: {
    background: '#ccc',
  },
}
export const App = () => {
  const [s, set] = useState(init)
  useEffect(() => {
    const u = pointer$.subscribe(v => {
      set(v)
      if (v.log) console.log('set')
    })
    return () => u.unsubscribe()
  }, [])
  const [[, c], add] = useReducer(
    ([i, p]: readonly [number, readonly string[]], c: string) =>
      [1 + i, [`${c} ${1 + i}`, ...p].slice(0, 20)] as const,
    [0, []],
  )
  useEffect(() => {
    const u = click$.subscribe(v => add(v))
    return () => u.unsubscribe()
  }, [])
  return (
    <div>
      <div style={styles.wrap} {...handlers}>
        <p>hoge</p>
        <p>fuga</p>
        <p>piyo</p>
      </div>
      <hr />
      <h4>state</h4>
      <pre>{JSON.stringify(s, null, 2)}</pre>
      <hr />
      <h4>clicks</h4>
      <pre>{c.join('\r\n')}</pre>
    </div>
  )
}
