/* eslint-disable import/no-deprecated */
import { render } from 'preact'
import { App } from './app'

render(<App />, document.body)
